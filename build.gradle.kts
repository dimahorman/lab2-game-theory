import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.5.10"
    id("java")
    application
}

group = "me.dima"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()

    maven {
        url = uri("https://dl.bintray.com/mipt-npm/dataforge")
    }

    maven {
        url = uri("https://repo.kotlin.link")
    }

    maven {
        url = uri("https://dl.bintray.com/kotlin/ktor/")
    }
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("space.kscience:plotlykt-server:0.5.0")
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "11"
}

application {
    mainClass.set("MainKt")
}