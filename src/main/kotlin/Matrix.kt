class Matrix<T : Number>(n: Int, m: Int, initRow: (Int) -> T = { 0 as T }, initializedArray: MutableList<MutableList<T>>? = null) {
    private val matrixList: MutableList<MutableList<T>>

    init {
        matrixList = initializedArray ?: MutableList(n) { MutableList(m, initRow) }
    }

    val indicesRows: IntRange
        get() = IntRange(0, matrixList.lastIndex)

    val indicesColumns: IntRange
        get() = IntRange(0, matrixList[0].lastIndex)

    val rowSize: Int
        get() = matrixList.size

    val columnSize: Int
        get() = matrixList[0].size

    operator fun get(n: Int, m: Int): T {
        return matrixList[n][m]
    }

    operator fun get(n: Int): MutableList<T> {
        return matrixList[n]
    }

    operator fun set(n: Int, m: Int, value: T) {
        matrixList[n][m] = value
    }
}
