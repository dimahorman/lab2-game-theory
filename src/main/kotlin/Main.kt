import MatrixGame.printMatrix
import space.kscience.dataforge.meta.invoke
import space.kscience.plotly.Plotly
import space.kscience.plotly.makeFile
import space.kscience.plotly.trace

fun main(args: Array<String>) {
    println("Hello World!")
    val matrix = MatrixGame.initializeMatrix(3, 3, -4, 5)

    println("Matrix: ")
    matrix.printMatrix()

    val yValues = MatrixGame.findBrownRobinson(matrix, 0, 0)
    val xValues = (0..8).map { it.toDouble() }

    val plot = Plotly.plot {
        trace {
            x.set(xValues)
            y.set(yValues)
            name = "for a single trace in graph its name would be hidden"
        }

        layout {
            title = "Graph name"
            xaxis {
                title = "x axis"
            }
            yaxis {
                title = "y axis"
            }
        }
    }

    plot.makeFile()
}