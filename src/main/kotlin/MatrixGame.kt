import java.util.*

object MatrixGame {

    private object PayoffCalculator {
        enum class PayoffType {
            A, B
        }

        private fun isPayoffTypeA(payoffType: PayoffType) = payoffType == PayoffType.A

        private fun <T : Number> getRanges(matrix: Matrix<T>, payoffType: PayoffType): Pair<IntRange, IntRange> {
            return if (isPayoffTypeA(payoffType)) {
                Pair(matrix.indicesRows, matrix.indicesColumns)
            } else {
                Pair(matrix.indicesColumns, matrix.indicesRows)
            }
        }

        private fun getPayoffResult(results: List<Double>, payoffType: PayoffType): Pair<Double, Int> {
            val maxOrMin = if (isPayoffTypeA(payoffType)) {
                results.maxOrNull() ?: throw RuntimeException("Could not find max element of empty array")
            } else {
                results.minOrNull() ?: throw RuntimeException("Could not find min element of empty array")
            }

            return Pair(maxOrMin, results.indexOfFirst { it == maxOrMin })
        }

        private fun <T : Number> getPayoffSumValue(
            matrix: Matrix<T>,
            empiricalVector: List<Double>,
            i: Int,
            j: Int,
            payoffType: PayoffType
        ): Double {
            val vectorValue = empiricalVector[j]
            return if (isPayoffTypeA(payoffType)) {
                matrix[i, j]
            } else {
                matrix[j, i]
            }.toDouble() * vectorValue
        }

        fun <T : Number> calculatePayoff(
            matrix: Matrix<T>,
            empiricalVector: List<Double>,
            payoffType: PayoffType
        ): Pair<Double, Int> {
            val results = ArrayList<Double>()
            val ranges = getRanges(matrix, payoffType)

            for (i in ranges.first) {
                var payoffSum = 0.0
                for (j in ranges.second) {
                    payoffSum += getPayoffSumValue(matrix, empiricalVector, i, j, payoffType)
                }
                results.add(payoffSum)
            }

            return getPayoffResult(results, payoffType)
        }
    }

    fun <T : Number> findBrownRobinson(matrix: Matrix<T>, iValue: Int, jValue: Int): List<Double> {
        val results = ArrayList<Double>()
        var i = iValue
        var j = jValue

        var p = Array(matrix.rowSize) { 0 }
            .mapIndexed { index, _ -> compareAndGetZeroOrOne(index, i).toDouble() }
            .toMutableList()

        var q = Array(matrix.columnSize) { 0 }
            .mapIndexed { index, _ -> compareAndGetZeroOrOne(index, j).toDouble() }
            .toMutableList()

        for (n in (1..matrix.rowSize * matrix.columnSize + 1)) {
            val payoffAResult = PayoffCalculator.calculatePayoff(matrix, q, PayoffCalculator.PayoffType.A)
            val payoffBResult = PayoffCalculator.calculatePayoff(matrix, p, PayoffCalculator.PayoffType.B)

            i = payoffAResult.second
            j = payoffBResult.second

            p = matrix.indicesRows.map {
                if (it == i) (p[it] * n + 1).div(n + 1) else (p[it] * n).div(n + 1)
            }.toMutableList()

            q = matrix.indicesColumns.map { if (it == j) (q[it] * n + 1).div(n + 1) else (q[it] * n).div(n + 1) }
                .toMutableList()

            val v = (payoffAResult.first + payoffBResult.first).div(2.0)
            results.add(v)
        }
        return results
    }

    private fun compareAndGetZeroOrOne(e1: Int, e2: Int) = if (e1 == e2) 1 else 0

    fun initializeMatrix(
        n: Int,
        m: Int,
        c1: Int,
        c2: Int,
    ): Matrix<Double> {
        return Matrix(n, m, initRow = { (c1..c2).random().toDouble() })
    }

    fun <T : Number> Matrix<T>.printMatrix() {
        for (i in this.indicesRows) {
            for (j in this[i]) {
                print(" ${j.toInt()}")
            }
            println("")
        }
    }
}